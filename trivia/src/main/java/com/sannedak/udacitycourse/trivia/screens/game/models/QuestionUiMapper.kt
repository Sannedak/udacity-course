package com.sannedak.udacitycourse.trivia.screens.game.models

import com.sannedak.udacitycourse.trivia.data.source.storage.models.Question

class QuestionUiMapper {

    fun map(questions: List<Question>) = questions.map { question ->
        QuestionUi(question.text, mapAnswers(question.answers), question.correctAnswer)
    }

    private fun mapAnswers(answers: List<String>): List<String> {
        val shuffledAnswers = answers.toMutableList().shuffled()
        //shuffledAnswers.shuffle()
        return shuffledAnswers
    }
}