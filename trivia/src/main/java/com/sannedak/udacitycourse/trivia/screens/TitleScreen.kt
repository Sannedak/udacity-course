package com.sannedak.udacitycourse.trivia.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.DrawerState
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ListItem
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Android
import androidx.compose.material.icons.outlined.Menu
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material.icons.outlined.Subject
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import com.sannedak.udacitycourse.trivia.R
import com.sannedak.udacitycourse.trivia.navigation.NavGraph
import com.sannedak.udacitycourse.trivia.ui.theme.TriviaTheme
import com.sannedak.udacitycourse.trivia.ui.theme.bigPadding
import com.sannedak.udacitycourse.trivia.ui.theme.drawerImageHeight
import com.sannedak.udacitycourse.trivia.ui.theme.halfPadding
import com.sannedak.udacitycourse.trivia.ui.theme.padding
import com.sannedak.udacitycourse.trivia.ui.theme.titleImageHeight
import java.util.Locale
import kotlinx.coroutines.launch

@Composable
fun TitleScreen(navController: NavHostController) {
    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { TitleAppBar(scaffoldState.drawerState, navController) },
        drawerContent = {
            TriviaAppDrawer(
                drawerState = scaffoldState.drawerState,
                navController = navController
            )
        }
    ) {
        TitleBody {
            scope.launch { scaffoldState.drawerState.close() }
            navController.navigate(NavGraph.Game.route)
        }
    }
}

@Composable
private fun TitleAppBar(drawerState: DrawerState, navController: NavHostController) {
    val scope = rememberCoroutineScope()

    TopAppBar(title = { Text(text = stringResource(id = R.string.title)) },
        navigationIcon = {
            IconButton(onClick = { scope.launch { drawerState.open() } }) {
                Icon(imageVector = Icons.Outlined.Menu, contentDescription = null)
            }
        },
        actions = {
            TriviaDropDownMenu {
                navController.navigate(NavGraph.About.route)
            }
        }
    )
}

@Composable
private fun TriviaDropDownMenu(onClick: () -> Unit) {
    var isExpanded by remember { mutableStateOf(false) }

    IconButton(onClick = { isExpanded = true }) {
        Icon(imageVector = Icons.Outlined.MoreVert, contentDescription = null)
    }
    DropdownMenu(expanded = isExpanded, onDismissRequest = { isExpanded = false }) {
        DropdownMenuItem(onClick = { onClick.invoke() }) {
            Text(text = stringResource(id = R.string.about))
        }
    }
}

@Composable
private fun TriviaAppDrawer(drawerState: DrawerState, navController: NavHostController) {
    val scope = rememberCoroutineScope()

    Column(modifier = Modifier.fillMaxSize()) {
        Surface(
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(drawerImageHeight)
        ) {
            Image(
                painter = painterResource(id = R.drawable.nav_header),
                contentDescription = null,
                modifier = Modifier.padding(
                    start = halfPadding + padding,
                    top = halfPadding + padding,
                    end = halfPadding + padding,
                    bottom = bigPadding + padding
                )
            )
        }
        Column {
            DrawerListItem(icon = Icons.Outlined.Subject, stringResource(id = R.string.rules)) {
                scope.launch {
                    drawerState.close()
                    navController.navigate(NavGraph.Rules.route)
                }
            }
            DrawerListItem(
                icon = Icons.Outlined.Android,
                itemText = stringResource(id = R.string.about)
            ) {
                scope.launch {
                    drawerState.close()
                    navController.navigate(NavGraph.About.route)
                }

            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun DrawerListItem(icon: ImageVector, itemText: String, onClick: () -> Unit) {
    ListItem(icon = { Icon(imageVector = icon, contentDescription = null) },
        text = { Text(text = itemText) },
        modifier = Modifier.clickable { onClick.invoke() }
    )
}

@Composable
private fun TitleBody(onClick: () -> Unit) {
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (image, button) = createRefs()

        Image(
            painter = painterResource(id = R.drawable.android_trivia),
            contentDescription = null,
            modifier = Modifier
                .height(titleImageHeight)
                .constrainAs(image) {
                    start.linkTo(parent.start, margin = halfPadding)
                    end.linkTo(parent.end, margin = halfPadding)
                    top.linkTo(parent.top)
                    bottom.linkTo(button.top)
                }
        )
        Button(
            onClick = { onClick.invoke() },
            modifier = Modifier.constrainAs(button) {
                    start.linkTo(parent.start, margin = halfPadding)
                    end.linkTo(parent.end, margin = halfPadding)
                    top.linkTo(image.bottom)
                    bottom.linkTo(parent.bottom)
                }
        ) {
            Text(text = stringResource(id = R.string.play).toUpperCase(Locale.ROOT))
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun TitleScreenPreview() {
    TriviaTheme {
        TitleScreen(rememberNavController())
    }
}