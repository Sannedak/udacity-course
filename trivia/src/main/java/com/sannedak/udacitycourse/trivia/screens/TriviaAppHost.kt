package com.sannedak.udacitycourse.trivia.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import com.sannedak.udacitycourse.trivia.navigation.NavGraph
import com.sannedak.udacitycourse.trivia.screens.game.TriviaGameScreen

@Composable
fun TriviaApp() {
    val navController = rememberNavController()

    Box(modifier = Modifier.fillMaxSize()) {
        NavHost(navController = navController, startDestination = NavGraph.Title.route) {
            composable(NavGraph.Title.route) { TitleScreen(navController = navController) }
            composable(NavGraph.About.route) { AboutTriviaScreen(navController = navController) }
            composable(NavGraph.Rules.route) { TriviaRulesScreen(navController = navController) }
            composable(NavGraph.Game.route) { TriviaGameScreen(navController = navController) }
            composable(
                "${NavGraph.GameWon.route}/{correctAnswers}",
                arguments = listOf(navArgument("correctAnswers") { type = NavType.IntType })
            ) { backStackEntry ->
                val correctAnswers = backStackEntry.arguments?.getInt("correctAnswers")
                GameWonScreen(navController, correctAnswers)
            }
            composable(NavGraph.GameOver.route) { GameOverScreen(navController = navController) }
        }
    }
}
