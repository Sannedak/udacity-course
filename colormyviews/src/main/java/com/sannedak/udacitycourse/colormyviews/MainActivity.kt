package com.sannedak.udacitycourse.colormyviews

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.sannedak.udacitycourse.colormyviews.screens.ViewsScreen
import com.sannedak.udacitycourse.colormyviews.ui.theme.ColorMyViewsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ColorMyViewsTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ViewsScreen()
                }
            }
        }
    }
}
