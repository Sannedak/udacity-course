package com.sannedak.udacitycourse.trivia.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.sannedak.udacitycourse.trivia.R
import com.sannedak.udacitycourse.trivia.ui.theme.TriviaTheme
import com.sannedak.udacitycourse.trivia.ui.theme.halfPadding
import com.sannedak.udacitycourse.trivia.ui.theme.padding
import com.sannedak.udacitycourse.trivia.ui.theme.rulesImageHeight

@Composable
fun TriviaRulesScreen(navController: NavHostController) {
    Column(modifier = Modifier.fillMaxSize()) {
        TopAppBar(title = { Text(text = stringResource(id = R.string.title)) },
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(imageVector = Icons.Outlined.ArrowBack, contentDescription = null)
                }
            })
        Image(
            painter = painterResource(id = R.drawable.trivia_rules),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(rulesImageHeight)
                .align(alignment = CenterHorizontally)
                .padding(start = halfPadding, top = padding, end = halfPadding)
        )
        Text(
            text = stringResource(id = R.string.trivia_rules),
            modifier = Modifier.padding(start = halfPadding, top = padding, end = halfPadding),
            style = typography.h5
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun RulesScreenPreview() {
    TriviaTheme {
        TriviaRulesScreen(rememberNavController())
    }
}