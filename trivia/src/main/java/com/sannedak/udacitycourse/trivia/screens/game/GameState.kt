package com.sannedak.udacitycourse.trivia.screens.game

enum class GameState {
    NO_ANSWER, WIN, DEFEAT, CORRECT_ANSWER
}