package com.sannedak.udacitycourse.colormyviews.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.sannedak.udacitycourse.colormyviews.R
import com.sannedak.udacitycourse.colormyviews.ui.theme.ColorMyViewsTheme
import com.sannedak.udacitycourse.colormyviews.ui.theme.Green
import com.sannedak.udacitycourse.colormyviews.ui.theme.HoloGreenDark
import com.sannedak.udacitycourse.colormyviews.ui.theme.HoloGreenLight
import com.sannedak.udacitycourse.colormyviews.ui.theme.Red
import com.sannedak.udacitycourse.colormyviews.ui.theme.Yellow
import com.sannedak.udacitycourse.colormyviews.ui.theme.boxTwoSize
import com.sannedak.udacitycourse.colormyviews.ui.theme.gameDescriptionStyle
import com.sannedak.udacitycourse.colormyviews.ui.theme.halfMargin
import com.sannedak.udacitycourse.colormyviews.ui.theme.margin
import com.sannedak.udacitycourse.colormyviews.ui.theme.whiteBoxTextStyle
import java.util.Locale

@Composable
fun ViewsScreen() {
    val coloredBoxes = remember {
        mutableStateMapOf(
            1 to Color.White,
            2 to Color.White,
            3 to Color.White,
            4 to Color.White,
            5 to Color.White
        )
    }

    Column {
        TopAppBar(title = { Text(text = stringResource(id = R.string.app_name)) })
        Boxes(coloredBoxes)
    }
}

@Composable
private fun Boxes(coloredBoxes: MutableMap<Int, Color>) {
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (boxOne, boxTwo, boxThree, boxFour, boxFive,
            textGameDescription, textGameRules, redButton, yellowButton, greenButton) = createRefs()

        createVerticalChain(boxThree, boxFour, boxFive, chainStyle = ChainStyle.SpreadInside)
        createHorizontalChain(redButton, yellowButton, greenButton, chainStyle = ChainStyle.Spread)

        val boxOneModifier = Modifier
            .constrainAs(boxOne) {
                start.linkTo(parent.start, margin = margin)
                end.linkTo(parent.end, margin = margin)
                top.linkTo(parent.top, margin = margin)
                width = Dimension.fillToConstraints
            }
        val boxTwoModifier = Modifier
            .constrainAs(boxTwo) {
                start.linkTo(parent.start, margin = margin)
                top.linkTo(boxOne.bottom, margin)
                bottom.linkTo(textGameDescription.top, margin = halfMargin)
                linkTo(boxOne.bottom, textGameDescription.top, topMargin = margin, bias = 0.0F)
            }
            .size(boxTwoSize)
        val boxThreeModifier = Modifier
            .constrainAs(boxThree) {
                start.linkTo(boxTwo.end, margin = margin)
                end.linkTo(parent.end, margin = margin)
                top.linkTo(boxTwo.top)
                bottom.linkTo(boxFour.top)
                width = Dimension.fillToConstraints
            }
        val boxFourModifier = Modifier
            .constrainAs(boxFour) {
                start.linkTo(boxTwo.end, margin = margin)
                end.linkTo(parent.end, margin = margin)
                top.linkTo(boxThree.bottom, margin = margin)
                bottom.linkTo(boxFive.top, margin = margin)
                width = Dimension.fillToConstraints
            }
        val boxFiveModifier = Modifier
            .constrainAs(boxFive) {
                start.linkTo(boxTwo.end, margin = margin)
                end.linkTo(parent.end, margin = margin)
                top.linkTo(boxFour.bottom)
                bottom.linkTo(boxTwo.bottom)
                width = Dimension.fillToConstraints
            }

        val textGameRulesModifier = Modifier
            .constrainAs(textGameRules) {
                start.linkTo(textGameDescription.end, margin = margin)
                end.linkTo(parent.end, margin = margin)
                top.linkTo(boxTwo.bottom, margin = margin)
                bottom.linkTo(parent.bottom, margin = halfMargin)
                linkTo(boxFive.bottom, parent.bottom, bias = 0.12F)
                linkTo(textGameDescription.end, parent.end, endMargin = margin, bias = 1F)
            }
        val textLabelModifier = Modifier
            .constrainAs(textGameDescription) {
                start.linkTo(parent.start, margin = margin)
                baseline.linkTo(textGameRules.baseline)
            }

        val redButtonModifier = Modifier
            .constrainAs(redButton) {
                start.linkTo(parent.start)
                end.linkTo(yellowButton.start)
                baseline.linkTo(yellowButton.baseline)
            }
        val yellowButtonModifier = Modifier
            .constrainAs(yellowButton) {
                start.linkTo(redButton.end)
                end.linkTo(greenButton.start)
                top.linkTo(textGameDescription.bottom, margin = halfMargin)
                bottom.linkTo(parent.bottom, margin = margin)
                linkTo(
                    textGameDescription.bottom,
                    parent.bottom,
                    bottomMargin = margin,
                    bias = 1.0F
                )
            }
        val greenButtonModifier = Modifier
            .constrainAs(greenButton) {
                start.linkTo(yellowButton.end)
                end.linkTo(parent.end)
                baseline.linkTo(yellowButton.baseline)
            }

        ColoredBox(
            color = coloredBoxes.getValue(1),
            modifier = boxOneModifier,
            boxText = stringResource(id = R.string.box_one)
        ) {
            coloredBoxes[1] = Color.DarkGray
        }
        ColoredBox(
            color = coloredBoxes.getValue(2),
            modifier = boxTwoModifier,
            boxText = stringResource(id = R.string.box_two)
        ) {
            coloredBoxes[2] = Color.Gray
        }
        ColoredBox(
            color = coloredBoxes.getValue(3),
            modifier = boxThreeModifier, boxText = stringResource(id = R.string.box_three)
        ) {
            coloredBoxes[3] = HoloGreenLight
        }
        ColoredBox(
            color = coloredBoxes.getValue(4),
            modifier = boxFourModifier,
            boxText = stringResource(id = R.string.box_four)
        ) {
            coloredBoxes[4] = HoloGreenDark
        }
        ColoredBox(
            color = coloredBoxes.getValue(5),
            modifier = boxFiveModifier,
            boxText = stringResource(id = R.string.box_five)
        ) {
            coloredBoxes[5] = HoloGreenLight
        }

        Text(
            text = stringResource(id = R.string.game_description),
            modifier = textLabelModifier,
            style = gameDescriptionStyle
        )
        Text(text = stringResource(id = R.string.game_rules), modifier = textGameRulesModifier)

        Button(onClick = { coloredBoxes[3] = Red }, modifier = redButtonModifier) {
            Text(text = stringResource(id = R.string.button_red).toUpperCase(Locale.ROOT))
        }
        Button(onClick = { coloredBoxes[4] = Yellow }, modifier = yellowButtonModifier) {
            Text(text = stringResource(id = R.string.button_yellow).toUpperCase(Locale.ROOT))
        }
        Button(onClick = { coloredBoxes[5] = Green }, modifier = greenButtonModifier) {
            Text(text = stringResource(id = R.string.button_green).toUpperCase(Locale.ROOT))
        }
    }
}

@Composable
private fun ColoredBox(color: Color, modifier: Modifier, boxText: String, onClick: () -> Unit) {
    Box(modifier = modifier
        .background(color)
        .clickable { onClick.invoke() }
    ) {
        Text(text = boxText, style = whiteBoxTextStyle, modifier = Modifier.fillMaxWidth())
    }
}

@Preview(showBackground = true)
@Composable
private fun ColoredBoxPreview() {
    ColorMyViewsTheme {
        ViewsScreen()
    }
}
