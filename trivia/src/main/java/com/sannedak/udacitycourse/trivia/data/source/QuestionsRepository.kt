package com.sannedak.udacitycourse.trivia.data.source

import com.sannedak.udacitycourse.trivia.data.source.storage.models.Question

interface QuestionsRepository {

    fun getQuestions():List<Question>
}