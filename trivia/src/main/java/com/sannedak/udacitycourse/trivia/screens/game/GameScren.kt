package com.sannedak.udacitycourse.trivia.screens.game

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.RadioButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.popUpTo
import androidx.navigation.compose.rememberNavController
import com.sannedak.udacitycourse.trivia.R
import com.sannedak.udacitycourse.trivia.data.source.QuestionsRepositoryImpl
import com.sannedak.udacitycourse.trivia.data.source.storage.QuestionsStorageService
import com.sannedak.udacitycourse.trivia.navigation.NavGraph
import com.sannedak.udacitycourse.trivia.screens.game.models.QuestionUiMapper
import com.sannedak.udacitycourse.trivia.ui.theme.AnswerStyle
import com.sannedak.udacitycourse.trivia.ui.theme.QuestionStyle
import com.sannedak.udacitycourse.trivia.ui.theme.TriviaTheme
import com.sannedak.udacitycourse.trivia.ui.theme.bigPadding
import com.sannedak.udacitycourse.trivia.ui.theme.gameImageHeight
import com.sannedak.udacitycourse.trivia.ui.theme.halfPadding
import com.sannedak.udacitycourse.trivia.ui.theme.padding
import java.util.Locale

@Composable
fun TriviaGameScreen(navController: NavHostController) {
    val gameViewModel: GameViewModel = viewModel(
        factory = GameViewModelFactory(
            QuestionsRepositoryImpl(QuestionsStorageService()),
            QuestionUiMapper()
        )
    )
    val currentQuestion by gameViewModel.currentQuestion.collectAsState()
    val gameState by gameViewModel.gameState.collectAsState()
    val questionIndex by gameViewModel.questionIndex.collectAsState()
    val context = LocalContext.current

    Column(modifier = Modifier.fillMaxSize()) {
        TopAppBar(title = {
            Text(
                text = stringResource(
                    id = R.string.in_game_app_bar_title,
                    questionIndex
                )
            )
        },
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(imageVector = Icons.Outlined.ArrowBack, contentDescription = null)
                }
            })
        Image(
            painter = painterResource(id = R.drawable.android_category_simple),
            contentDescription = null,
            modifier = Modifier
                .height(gameImageHeight)
                .align(CenterHorizontally)
                .padding(start = halfPadding, top = padding, end = halfPadding, bottom = padding)
        )
        Question(question = currentQuestion.question)
        Answers(answers = currentQuestion.answers) { gameViewModel.onSelectAnswer(it) }
        Button(
            onClick = {
                gameViewModel.onSubmit()
                when (gameState) {
                    GameState.NO_ANSWER -> Toast.makeText(
                        context,
                        context.getString(R.string.no_answer),
                        Toast.LENGTH_SHORT
                    ).show()
                    GameState.WIN -> {
                        navController.navigate("${NavGraph.GameWon.route}/$questionIndex") {
                            popUpTo(NavGraph.Game.route) { inclusive = true }
                        }
                    }
                    GameState.DEFEAT -> {
                        navController.navigate(NavGraph.GameOver.route) {
                            popUpTo(NavGraph.Game.route) { inclusive = true }
                        }
                    }
                    GameState.CORRECT_ANSWER -> Toast.makeText(
                        context,
                        context.getString(R.string.correct),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            },
            modifier = Modifier
                .padding(top = halfPadding)
                .align(CenterHorizontally)
        ) {
            Text(text = stringResource(id = R.string.submit).toUpperCase(Locale.ROOT))
        }
    }
}

@Composable
private fun Question(question: String) {
    Text(
        text = question,
        style = QuestionStyle,
        modifier = Modifier.padding(
            start = bigPadding,
            top = padding,
            end = bigPadding,
            bottom = padding
        )
    )
}

@Composable
private fun Answers(answers: List<String>, onClick: (String) -> Unit) {
    val (selectedAnswer, onOptionSelected) = remember { mutableStateOf(answers[0]) }

    Column(modifier = Modifier.padding(start = padding, end = padding, bottom = halfPadding)) {
        answers.forEach { answer ->
            Row(
                verticalAlignment = CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .selectable(
                        selected = (answer == selectedAnswer),
                        onClick = {
                            onOptionSelected(answer)
                            onClick.invoke(answer)
                        },
                        interactionSource = MutableInteractionSource(),
                        indication = null
                    )
                    .padding(vertical = halfPadding)
            ) {
                RadioButton(
                    selected = (answer == selectedAnswer),
                    onClick = { onOptionSelected(answer) },
                    modifier = Modifier.padding(start = padding)
                )
                Text(
                    text = answer,
                    style = AnswerStyle,
                    modifier = Modifier.padding(start = padding)
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun GameScreenPreview() {
    TriviaTheme {
        TriviaGameScreen(rememberNavController())
    }
}