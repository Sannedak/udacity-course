package com.sannedak.udacitycourse.trivia.screens.game.models

data class QuestionUi(
    val question: String,
    val answers: List<String>,
    val correctAnswer: String
)