package com.sannedak.udacitycourse.trivia.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import com.sannedak.udacitycourse.trivia.R
import com.sannedak.udacitycourse.trivia.navigation.NavGraph
import com.sannedak.udacitycourse.trivia.ui.theme.TriviaTheme
import com.sannedak.udacitycourse.trivia.ui.theme.gameOverBackground
import com.sannedak.udacitycourse.trivia.ui.theme.halfPadding
import com.sannedak.udacitycourse.trivia.ui.theme.padding
import com.sannedak.udacitycourse.trivia.ui.theme.winImageHeight
import java.util.Locale

@Composable
fun GameOverScreen(navController: NavHostController) {
    Column(modifier = Modifier.fillMaxSize()) {
        TopAppBar(title = { Text(text = stringResource(id = R.string.title)) },
            navigationIcon = {
                IconButton(onClick = { navController.navigate(NavGraph.Title.route) }) {
                    Icon(imageVector = Icons.Outlined.ArrowBack, contentDescription = null)
                }
            }
        )
        GameOverBody {
            navController.navigate(NavGraph.Title.route)
        }
    }
}

@Composable
private fun GameOverBody(onClick: () -> Unit) {
    Box(modifier = Modifier.background(gameOverBackground)) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (image, nextMatchButton) = createRefs()

            Image(
                painter = painterResource(id = R.drawable.try_again),
                contentDescription = null,
                modifier = Modifier
                    .height(winImageHeight)
                    .constrainAs(image) {
                        start.linkTo(parent.start, margin = halfPadding)
                        end.linkTo(parent.end, margin = halfPadding)
                        top.linkTo(parent.top, margin = halfPadding)
                        bottom.linkTo(nextMatchButton.top, margin = padding)
                    }
            )
            Button(onClick = onClick,
                modifier = Modifier.constrainAs(nextMatchButton) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(image.bottom)
                        bottom.linkTo(parent.bottom)
                    }) {
                Text(text = stringResource(id = R.string.try_again).toUpperCase(Locale.ROOT))
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun GameOverScreenPreview() {
    TriviaTheme {
        GameOverScreen(rememberNavController())
    }
}
