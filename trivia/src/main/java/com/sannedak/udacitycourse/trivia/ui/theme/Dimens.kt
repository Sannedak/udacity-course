package com.sannedak.udacitycourse.trivia.ui.theme

import androidx.compose.ui.unit.dp

val drawerImageHeight = 192.dp
val titleImageHeight = 192.dp
val aboutImageHeight = 192.dp
val rulesImageHeight = 192.dp
val gameImageHeight = 192.dp
val winImageHeight = 350.dp

val bigPadding = 32.dp
val padding = 16.dp
val halfPadding = 8.dp