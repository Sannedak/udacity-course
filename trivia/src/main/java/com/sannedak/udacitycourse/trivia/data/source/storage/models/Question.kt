package com.sannedak.udacitycourse.trivia.data.source.storage.models

data class Question(
    val text: String,
    val answers: List<String>,
    val correctAnswer: String
)
