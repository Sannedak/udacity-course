package com.sannedak.udacitycourse.trivia.screens

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.material.icons.outlined.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import androidx.navigation.compose.rememberNavController
import com.sannedak.udacitycourse.trivia.R
import com.sannedak.udacitycourse.trivia.navigation.NavGraph
import com.sannedak.udacitycourse.trivia.ui.theme.TriviaTheme
import com.sannedak.udacitycourse.trivia.ui.theme.halfPadding
import com.sannedak.udacitycourse.trivia.ui.theme.padding
import com.sannedak.udacitycourse.trivia.ui.theme.winBackground
import com.sannedak.udacitycourse.trivia.ui.theme.winImageHeight
import java.util.Locale

@Composable
fun GameWonScreen(navController: NavHostController, correctAnswers: Int?) {
    val context = LocalContext.current

    Column(modifier = Modifier.fillMaxSize()) {
        TopAppBar(title = { Text(text = stringResource(id = R.string.title)) },
            navigationIcon = {
                IconButton(onClick = { navController.navigate(NavGraph.Title.route) }) {
                    Icon(imageVector = Icons.Outlined.ArrowBack, contentDescription = null)
                }
            },
            actions = {
                IconButton(onClick = { share(context, correctAnswers) }) {
                    Icon(imageVector = Icons.Outlined.Share, contentDescription = null)
                }
            }
        )
        GameWonBody {
            navController.navigate(NavGraph.Title.route)
        }
    }
}

@Composable
private fun GameWonBody(onClick: () -> Unit) {
    Box(modifier = Modifier.background(winBackground)) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (image, nextMatchButton) = createRefs()

            Image(
                painter = painterResource(id = R.drawable.you_win),
                contentDescription = null,
                modifier = Modifier
                    .height(winImageHeight)
                    .constrainAs(image) {
                        start.linkTo(parent.start, margin = halfPadding)
                        end.linkTo(parent.end, margin = halfPadding)
                        top.linkTo(parent.top, margin = halfPadding)
                        bottom.linkTo(nextMatchButton.top, margin = padding)
                    }
            )
            Button(onClick = onClick,
                modifier = Modifier.constrainAs(nextMatchButton) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(image.bottom)
                        bottom.linkTo(parent.bottom)
                    }) {
                Text(text = stringResource(id = R.string.try_again).toUpperCase(Locale.ROOT))
            }
        }
    }
}

private fun share(context: Context, correctAnswers: Int?) {
    val sendIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_success_text, correctAnswers))
        type = "text/plain"
    }
    val intent = Intent.createChooser(sendIntent, null)
    startActivity(context, intent, null)
}

@Preview(showBackground = true)
@Composable
private fun GameWonScreenPreview() {
    TriviaTheme {
        GameWonScreen(rememberNavController(), null)
    }
}