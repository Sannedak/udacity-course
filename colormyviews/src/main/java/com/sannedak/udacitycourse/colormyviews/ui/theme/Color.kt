package com.sannedak.udacitycourse.colormyviews.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val HoloGreenLight = Color(0xFF99CC00)
val HoloGreenDark = Color(0xFF669900)
val Red = Color(0xFFE54304)
val Yellow = Color(0xFFFFC107)
val Green = Color(0xFF12C700)