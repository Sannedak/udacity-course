package com.sannedak.udacitycourse.diceroller.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.sannedak.udacitycourse.diceroller.R
import com.sannedak.udacitycourse.diceroller.ui.theme.DiceRollerTheme

@Composable
fun DiceRollerScreen(viewModel: DiceRollerViewModel = viewModel()) {
    val diceNumber by viewModel.diceNumber.collectAsState()

    Column(modifier = Modifier.fillMaxSize()) {
        TopAppBar(title = { Text(text = stringResource(id = R.string.app_name)) })
        DiceRoller(diceNumber = diceNumber, onDiceRoll = { viewModel.rollDice() })
    }
}

@Composable
private fun DiceRoller(diceNumber: Int, onDiceRoll: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = diceNumber),
            contentDescription = stringResource(id = R.string.dice_image_description)
        )
        Button(onClick = onDiceRoll) {
            Text(text = stringResource(id = R.string.button_roll))
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DiceRollerPreview() {
    val diceNumber = R.drawable.dice_3
    DiceRollerTheme {
        DiceRoller(diceNumber = diceNumber) {}
    }
}