package com.sannedak.udacitycourse.trivia.screens.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.udacitycourse.trivia.data.source.QuestionsRepository
import com.sannedak.udacitycourse.trivia.screens.game.models.QuestionUiMapper

class GameViewModelFactory(
    private val repository: QuestionsRepository,
    private val mapper: QuestionUiMapper
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GameViewModel(repository, mapper) as T
    }
}