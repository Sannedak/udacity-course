package com.sannedak.udacitycourse.trivia.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val winBackground = Color(0xFFB7F8C9)
val gameOverBackground = Color(0xFFF8BBD0)
