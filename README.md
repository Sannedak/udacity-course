# About repository
Application ideas taken from this course: [Developing Android Apps with Kotlin](https://www.udacity.com/course/developing-android-apps-with-kotlin--ud9012).
In this course you can learn to architect and develop Android apps in the Kotlin programming language using industry-proven tools and libraries.

Moreover, all applications are written using [Jetpack Compose](https://developer.android.com/jetpack/compose)

# Working with repository code
1. Download [Android Studio Preview ](https://developer.android.com/studio/preview)  (repo updated for use with Artic Fox 2020.3.1 Canary 14)
2. Clone repository
   ```bash
   git clone https://gitlab.com/Sannedak/udacity-course.git
   ```
3. Choose app from configuration list in IDE (diceroller, colormyviews, trivia)
4. Build app
5. Launch on smartphone or emulator with Android 5.0+


## Dice Roller
Dice Roller is a simple app that rolls a six sided die.

### Screenshots
|Light Theme                   |Dark Theme                   |
|------------------------------|-----------------------------|
|<img src="screenshots/diceroller/Screenshot_light.png" height="400">            |<img src="screenshots/diceroller/Screenshot_dark.png" height="400">             | 

## Color My Views
The ColorMyViews app is a demo game app that lets users click to color boxes and the background.
This app demonstrates the following views and techniques:
-   Using compose to create layout
-   Using constraint layout without Layout Editor

### Screenshots
|Initial Screen       |Colored by taps       | Colored by buttons      |
|---------------------|----------------------|-------------------------|
|<img src="screenshots/colormyviews/Initial_screen.png" height="400"> |<img src="screenshots/colormyviews/colored_by_taps.png" height="400">|<img src="screenshots/colormyviews/colored_by_buttons.png" height="400">|

## Trivia
The Trivia application is an application that asks the user trivia questions about Android development. It makes use of the compose Navigation to move the user between different screens. Each screen is implemented as a Fragment. The app navigates using buttons, the Action Bar, and the Navigation Drawer.

### Screenshots
|Main Screen       |Game Screen       | Game Won Screen      |
|------------------|------------------|----------------------|
|<img src="screenshots/trivia/main_screen.png" height="400">|<img src="screenshots/trivia/game_screen.png" height="400">|<img src="screenshots/trivia/game_won_screen.png" height="400">|