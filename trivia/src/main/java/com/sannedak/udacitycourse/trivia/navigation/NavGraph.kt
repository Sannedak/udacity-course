package com.sannedak.udacitycourse.trivia.navigation

sealed class NavGraph(val route: String) {
    object Title : NavGraph("trivia")
    object About : NavGraph("about")
    object Rules : NavGraph("rules")
    object Game : NavGraph("game")
    object GameWon : NavGraph("gameWon")
    object GameOver : NavGraph("gameOver")
}