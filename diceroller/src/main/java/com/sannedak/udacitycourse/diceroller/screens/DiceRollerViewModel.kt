package com.sannedak.udacitycourse.diceroller.screens

import androidx.lifecycle.ViewModel
import com.sannedak.udacitycourse.diceroller.R
import kotlin.random.Random
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class DiceRollerViewModel : ViewModel() {

    private var _diceNumber = MutableStateFlow(R.drawable.dice_6)

    val diceNumber = _diceNumber.asStateFlow()

    fun rollDice() {
        _diceNumber.value = when (Random.nextInt(6)) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
    }
}