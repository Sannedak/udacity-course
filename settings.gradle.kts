dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        jcenter() // Warning: this repository is going to shut down soon
    }
}

enableFeaturePreview("VERSION_CATALOGS")

rootProject.name = "Udacity Course"
include(":app")
include(":diceroller")
include(":colormyviews")
include(":trivia")
