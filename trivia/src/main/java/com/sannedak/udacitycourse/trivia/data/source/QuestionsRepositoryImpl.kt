package com.sannedak.udacitycourse.trivia.data.source

import com.sannedak.udacitycourse.trivia.data.source.storage.QuestionsStorageService

class QuestionsRepositoryImpl(
    private val storageService: QuestionsStorageService
) : QuestionsRepository {

    override fun getQuestions() = storageService.getStorageQuestions()
}