package com.sannedak.udacitycourse.trivia.screens.game

import androidx.lifecycle.ViewModel
import com.sannedak.udacitycourse.trivia.data.source.QuestionsRepository
import com.sannedak.udacitycourse.trivia.screens.game.models.QuestionUi
import com.sannedak.udacitycourse.trivia.screens.game.models.QuestionUiMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class GameViewModel(
    private val repository: QuestionsRepository,
    private val mapper: QuestionUiMapper
) : ViewModel() {

    private val questions = getThreeQuestions()
    private var currentAnswer = ""

    private val _gameState = MutableStateFlow(GameState.NO_ANSWER)
    val gameState = _gameState.asStateFlow()

    private val _questionIndex = MutableStateFlow(0)
    val questionIndex = _questionIndex.asStateFlow()

    private val _currentQuestion = MutableStateFlow(questions[_questionIndex.value])
    val currentQuestion = _currentQuestion.asStateFlow()

    fun onSelectAnswer(answer: String) {
        if (answer != _currentQuestion.value.correctAnswer) {
            _gameState.value = GameState.DEFEAT
        } else {
            if (currentAnswer != answer) {
                currentAnswer = answer
                _gameState.value = GameState.CORRECT_ANSWER
                if(_questionIndex.value == 2) {
                    _questionIndex.value++
                    _gameState.value = GameState.WIN
                }
            } else {
                _gameState.value = GameState.NO_ANSWER
            }
        }
    }

    fun onSubmit() {
        if (_gameState.value == GameState.CORRECT_ANSWER) {
            _questionIndex.value++
            _currentQuestion.value = questions[_questionIndex.value]
        }
    }

    private fun getThreeQuestions(): List<QuestionUi> {
        val questions = mapper.map(repository.getQuestions()).shuffled()
        return questions.take(3)
    }
}
