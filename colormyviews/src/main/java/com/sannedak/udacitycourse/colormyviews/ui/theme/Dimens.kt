package com.sannedak.udacitycourse.colormyviews.ui.theme

import androidx.compose.ui.unit.dp

val margin = 16.dp
val halfMargin = 8.dp

val boxTwoSize = 130.dp